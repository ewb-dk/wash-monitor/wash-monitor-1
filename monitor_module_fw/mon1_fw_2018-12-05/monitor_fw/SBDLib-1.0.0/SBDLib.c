/***************************************************************************
# Iridium Short Burst Data library
# Copyright (c) 2018, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
Revision
2018-02-13 KJ First released version

****************************************************************************/
/* parameters */
/* #define DEBUG  */

/***************************************************************************/
/* includes */
#include <string.h>
#include <SBDLib.h>

#ifdef DEBUG
#include <stdio.h>
#endif

/***************************************************************************/
 
/* defines */
#define RXBUF_SIZE                  400
#define MSG_SIZE                    50
#define TRY_MAX                     3


/* sbdlib auto states */
#define SBD_AUTO_IDLE                0

#define SBD_AUTO_SEND_TXT_CFG        1
#define SBD_AUTO_SEND_TXT_CFG_WAIT   2
#define SBD_AUTO_SEND_TXT_RSSI       3
#define SBD_AUTO_SEND_TXT_SEND       4

/* sbdlib states */
#define SBD_STATE_IDLE               0

#define SBD_STATE_CFG_REQ            1
#define SBD_STATE_CFG_REQ_WAIT       2

#define SBD_STATE_RSSI_REQ           3
#define SBD_STATE_RSSI_REQ_WAIT      4 

#define SBD_STATE_SEND_TXT_CPY       5
#define SBD_STATE_SEND_TXT_CPY_WAIT  6 
#define SBD_STATE_SEND_TXT_SYNC      7 
#define SBD_STATE_SEND_TXT_SYNC_WAIT 8 

#define SBD_STATE_SYNC_BIN_TRY       9
#define SBD_STATE_SYNC_BIN_RDY_WAIT  10 

/***************************************************************************/
/* static variables */

static short i, j; /* must be signed because of for loop down to 0 */
static unsigned char rxbuf[RXBUF_SIZE];
static short rxbuf_len;
static unsigned char msg[MSG_SIZE];
static unsigned short msg_len;
static char sbd_state, sbd_auto;
static unsigned char try;
static unsigned long tout;
static unsigned long secs;
static char return_code;
static char sbd_result;
static char rssi;

/***************************************************************************/
/* static function prototypes */
static short sbd_rxbuf_get_first_line_end(void);
static void sbd_rxbuf_remove_first_line(void);
static void sbd_rx_reset(void);
#ifdef DEBUG
static void sbd_debug_print_rxbuf (void);
#endif
static void sbd_modem_cfg_update(void);
static void sbd_get_rssi_update(void);
static void sbd_send_txt_update(void);
static void sbd_sync_bin(void);
static void sbd_auto_send_txt_update(void);

/***************************************************************************/
void sbd_init(unsigned long seconds)
{
	secs = seconds; /* make sure we set the time before any other call */
	sbd_state = SBD_STATE_IDLE;
}
/***************************************************************************/
void sbd_modem_cfg(void)
{
  try = 0;
  sbd_state = SBD_STATE_CFG_REQ;
  return_code = SBD_RETURN_WAIT;
}
/***************************************************************************/
static void sbd_modem_cfg_update(void)
{
	switch (sbd_state)
	{
		case SBD_STATE_CFG_REQ: /* Disable flow control and local echo */
			try++;
			sbd_rx_reset();
			sbd_tx("AT&K0E0\r");
			tout = secs + 4; 
			sbd_state = SBD_STATE_CFG_REQ_WAIT;
			break;

		case SBD_STATE_CFG_REQ_WAIT: /* wait... */
			if (secs >= tout) /* timeout check */
			{
				#ifdef DEBUG
				printf ("timeout!\n");
				#endif
				if (try < TRY_MAX)
					sbd_state = SBD_STATE_CFG_REQ;
				else
				{
					return_code = SBD_RETURN_ERR;
					sbd_state = SBD_STATE_IDLE;
				}
			}
			else /* incoming data check */
			{
				while (sbd_rxbuf_get_first_line_end() != -1) /* incoming data check */
				{
					#ifdef DEBUG
					printf ("Received: %s\n", rxbuf);	
					#endif
					if (strstr((char *) rxbuf, "OK"))
          {
						return_code = SBD_RETURN_OK;
            sbd_state = SBD_STATE_IDLE;
          }
					sbd_rxbuf_remove_first_line();
				}
			}
			break;
	}
}
/***************************************************************************/
void sbd_get_rssi(void)
{
  try = 0;
  sbd_state = SBD_STATE_RSSI_REQ;
  return_code = SBD_RETURN_WAIT;
}
/***************************************************************************/
static void sbd_get_rssi_update (void)
{
	switch (sbd_state)
	{
		case SBD_STATE_RSSI_REQ: /* request rssi */
			try++;
			sbd_rx_reset();
			sbd_tx("AT+CSQ\r");
			tout = secs + 12; 
			sbd_state = SBD_STATE_RSSI_REQ_WAIT;
			break;

		case SBD_STATE_RSSI_REQ_WAIT: /* wait for response */
			if (secs >= tout) /* timeout check */
			{
				#ifdef DEBUG
				printf ("timeout!\n");
				#endif
				if (try < TRY_MAX)
					sbd_state = SBD_STATE_RSSI_REQ;
				else
				{
					return_code = SBD_RETURN_ERR;
					sbd_state = SBD_STATE_IDLE;
				}
			}
			else 
			{
				while (sbd_rxbuf_get_first_line_end() != -1) /* incoming data check */
				{
					#ifdef DEBUG
					printf ("Received line: %s\n", rxbuf);
					#endif
					if (rxbuf_len >= 6 && rxbuf[0] == '+' && rxbuf[1] == 'C'
					  && rxbuf[2] == 'S' && rxbuf[3] == 'Q' && rxbuf[4] == ':'
					  && rxbuf[5] >= '0' && rxbuf[5] <= '5')
					{
						rssi = rxbuf[5] - '0';
						return_code = SBD_RETURN_OK;
            sbd_state = SBD_STATE_IDLE;
					}
					sbd_rxbuf_remove_first_line();
				}
			}
			break;
	}
}
/***************************************************************************/
char sbd_rssi(void)
{
	return rssi;
}
/***************************************************************************/
void sbd_send_txt(char *m, unsigned short m_len)
{
	memcpy (msg, m, m_len+1); /* remember to include the trailing zero */
	/* msg_len = strlen(m); */
	try = 0;
	sbd_state = SBD_STATE_SEND_TXT_CPY;
  return_code = SBD_RETURN_WAIT;
}
/***************************************************************************/
static void sbd_send_txt_update(void)
{
	switch (sbd_state)
	{
		case SBD_STATE_SEND_TXT_CPY: /* send txt */
			try++;
			sbd_rx_reset();
			sbd_tx("AT+SBDWT=");
			sbd_tx((char *) msg);
			sbd_tx("\r");
			#ifdef DEBUG
			printf ("Sending: AT+SBDWT=%s\n", (char *) msg);
			#endif
			tout = secs + 3; 
			sbd_state = SBD_STATE_SEND_TXT_CPY_WAIT;
			break;

		case SBD_STATE_SEND_TXT_CPY_WAIT: /* wait for response */
			if (secs >= tout) /* timeout check */
			{
				#ifdef DEBUG
				printf ("timeout!\n");
				#endif
				if (try < TRY_MAX)
					sbd_state = SBD_STATE_SEND_TXT_CPY;
				else
				{
					return_code = SBD_RETURN_ERR;
					sbd_state = SBD_STATE_IDLE;
				}
			}
			else 
			{
				while (sbd_rxbuf_get_first_line_end() != -1) /* incoming data check */
				{
					#ifdef DEBUG
					printf ("Received: %s\n", rxbuf);	
					#endif
					if (strstr((char *) rxbuf, "OK"))
					{
						sbd_state = SBD_STATE_SEND_TXT_SYNC;
						try = 0;
					}
					sbd_rxbuf_remove_first_line();
				}
			}
			break;

		case SBD_STATE_SEND_TXT_SYNC: /* synchronize */
			try++;
			sbd_rx_reset();
			sbd_tx("AT+SBDIX\r");
			#ifdef DEBUG
			printf ("Sending: AT+SBDIX\n");
			#endif
			tout = secs + 60; 
			sbd_state = SBD_STATE_SEND_TXT_SYNC_WAIT;
			break;

		case SBD_STATE_SEND_TXT_SYNC_WAIT: /* wait for response */
			if (secs >= tout) /* timeout check */
			{
				#ifdef DEBUG
				printf ("timeout!\n");
				#endif
				if (try < TRY_MAX)
					sbd_state = SBD_STATE_SEND_TXT_SYNC;
				else
				{
					return_code = SBD_RETURN_ERR;
				}
			}
			else 
			{
				while (sbd_rxbuf_get_first_line_end() != -1) /* incoming data check */
				{
					#ifdef DEBUG
					printf ("Received: %s\n", rxbuf);
					#endif
					if (rxbuf[0] == '+' && rxbuf[5] == 'X') /* simple check for +SBDIX */
					{
						unsigned char mo;

						/* read MO status (dirty hack) */
						mo = rxbuf[8] - '0';
						if (rxbuf[9] != ',')
							mo = mo*10 + (rxbuf[9] - '0');
			
						if (mo == 0)
							return_code = SBD_RETURN_OK;
 						else
							return_code = SBD_RETURN_ERR;								
                                                                                            						sbd_state = SBD_STATE_IDLE;
					}
					sbd_rxbuf_remove_first_line();
				}
			}
			break;
	}
}
/***************************************************************************/
void sbd_send_msg_bin(unsigned char *m, unsigned short m_len)
{
	memcpy (msg, m, m_len);
	msg_len = m_len;
	try = 0;
	sbd_state = SBD_STATE_SYNC_BIN_TRY;
  return_code = SBD_RETURN_WAIT;
}
/***************************************************************************/
void sbd_sync_bin(void)
{
	switch (sbd_state)
	{
		case SBD_STATE_SYNC_BIN_TRY: /* request rssi */
			try++;
			sbd_rx_reset();
			sbd_tx("AT+CSQ");
			tout = secs + 3; 
			sbd_state = SBD_STATE_SYNC_BIN_RDY_WAIT;
			break;

		case SBD_STATE_SYNC_BIN_RDY_WAIT: /* wait for response */
			if (secs >= tout) /* timeout check */
			{
				#ifdef DEBUG
				printf ("timeout!\n");
				#endif
				if (try < TRY_MAX)
					sbd_state = SBD_STATE_SYNC_BIN_TRY;
				else
				{
					return_code = SBD_RETURN_ERR;
					sbd_state = SBD_STATE_IDLE;
				}
			}
			else 
			{
				while (sbd_rxbuf_get_first_line_end() -! -1) /* incoming data check */
				{
					if (strstr((char *) rxbuf, "READY"))
					{


					}
					sbd_rxbuf_remove_first_line();
				}
			}
			break;
	}

}
/***************************************************************************/
void sbd_rx_byte(unsigned char b)
{
  if ((rxbuf_len + 1) < RXBUF_SIZE)
     rxbuf[rxbuf_len++] = b;
  else
    sbd_rx_reset();
}
/***************************************************************************/
void sbd_rx(unsigned char *b, unsigned short b_len)
{
	if ((b_len + rxbuf_len) < RXBUF_SIZE)
	{
		memcpy (rxbuf + rxbuf_len, b, b_len);
		rxbuf_len += b_len; 
	}
	else
		sbd_rx_reset();
}
/***************************************************************************/
short sbd_rxbuf_get_first_line_end(void)
{
	short line_end = -1;

	/* find first line end char */
	for (i=0; i<rxbuf_len; i++)
	{
		if ((rxbuf[i] == 13 || rxbuf[i] == 0) && line_end == -1)
		{
			line_end = i;
			rxbuf[i] = 0; /* convert to NULL to enable string functions */
		}
	}

	/*if (line_end != -1)
	{
		printf ("get line end %d\n", line_end);
		sbd_debug_print_rxbuf();
	}*/
	return line_end;
}
/***************************************************************************/
void sbd_rxbuf_remove_first_line(void)
{
	short line_end = -1;

	/* find first line end char */
	for (i=0; i<rxbuf_len; i++)
	{
		if (line_end == -1 && (rxbuf[i] == 0 || rxbuf[i] == 13 || rxbuf[i] == 10))
			line_end = i;
	}
	i = line_end;

	if (i != -1)
	{
		/* include any neighbour line end chars */
		while (i<rxbuf_len && (rxbuf[i+1] == 13 || rxbuf[i+1] == 10 || rxbuf[i+1] == 0))
			i++;

			/* go to first char in the next line */
		if (i <rxbuf_len)
			i++;
 
		/* now delete from buffer */
		rxbuf_len -= i;
		for (j=0; j<rxbuf_len; i++, j++)
		{
			rxbuf[j] = rxbuf[i];
		}
		/* printf ("after removal: ");
		sbd_debug_print_rxbuf(); */

	}
}
/***************************************************************************/
static void sbd_rx_reset(void)
{
	rxbuf_len = 0;
}
/***************************************************************************/
#ifdef DEBUG
void sbd_debug_print_rxbuf (void)
{
	if (rxbuf_len > 0)
	{
		printf ("RXBUF: ");
		for (i=0; i<rxbuf_len; i++)
		{
			if (rxbuf[i] == 0)
				printf ("[NULL]");
			else if (rxbuf[i] == 10)
				printf ("[LF]");
			else if (rxbuf[i] == 13)
				printf ("[CR]");
			else
				printf ("%c", rxbuf[i]);
		}
	printf ("[END] (length %d)\n", rxbuf_len);
	}
}
#endif
/***************************************************************************/
void sbd_auto_send_txt(char *m, unsigned short m_len)
{
	memcpy (msg, m, m_len+1); /* remember to include the trailing zero */
	try = 0;
	sbd_state = SBD_STATE_CFG_REQ;
	sbd_auto = SBD_AUTO_SEND_TXT_CFG;
	return_code = SBD_RETURN_WAIT;
}
/***************************************************************************/
static void sbd_auto_send_txt_update (void)
{
	switch (sbd_auto)
	{
		case SBD_AUTO_SEND_TXT_CFG:
			sbd_modem_cfg();
			sbd_auto = SBD_AUTO_SEND_TXT_CFG_WAIT;

		case SBD_AUTO_SEND_TXT_CFG_WAIT:
			if (return_code != SBD_RETURN_WAIT)
			{
				#ifdef DEBUG
				if (return_code == SBD_RETURN_OK)
					printf ("Iridium modem configured OK\n");
				else if (return_code == SBD_RETURN_ERR)
					printf ("Iridium modem configure ERROR\n");
				else
					printf ("Unknown return code: %d\n", return_code);
				#endif

				sbd_state = SBD_STATE_IDLE;
				if (return_code == SBD_RETURN_OK)
				{
					sbd_auto = SBD_AUTO_SEND_TXT_RSSI;
					sbd_get_rssi();
				}
				else
				{
					sbd_auto = SBD_AUTO_IDLE;
					return_code = SBD_RETURN_ERR;
				}
			}

			break;
			
		case SBD_AUTO_SEND_TXT_RSSI:
			if (return_code != SBD_RETURN_WAIT)
			{
				#ifdef DEBUG
				if (rssi >= 0)
					printf ("RSSI: %d%%\n", rssi*20);
				else if (return_code == SBD_RETURN_ERR)
					printf ("Get RSSI ERROR\n");
				else
					printf ("Unknown return code: %d\n", return_code);
				#endif

				sbd_state = SBD_STATE_IDLE;
				if (return_code == SBD_RETURN_OK) /* RSSI received ok and > 40% */
				{
					if (rssi >= 2)
					{
						sbd_auto = SBD_AUTO_SEND_TXT_SEND;
						sbd_send_txt((char *) msg, msg_len);
					}
					else
					{
						sbd_auto = SBD_AUTO_IDLE;
						return_code = SBD_RETURN_ERR_LINK;					
					}
				}
				else
				{
					sbd_auto = SBD_AUTO_IDLE;
					return_code = SBD_RETURN_ERR;
				}
			}
			break;
			
		case SBD_AUTO_SEND_TXT_SEND:
			if (return_code != SBD_RETURN_WAIT)
			{
				#ifdef DEBUG
				if (return_code == SBD_RETURN_OK)
					printf ("SBD send text msg OK\n");
				else if (return_code == SBD_RETURN_ERR)
					printf ("SBD send text msg ERROR\n");
				else
					printf ("Unknown return code: %d\n", return_code);
				#endif

				sbd_state = SBD_STATE_IDLE;
				if (return_code == SBD_RETURN_OK)
				{
					sbd_auto = SBD_AUTO_IDLE;
				}
				else
				{
					sbd_auto = SBD_AUTO_IDLE;
				}
			}
			break;
	}
}
/***************************************************************************/
char sbd_update(unsigned long seconds)
{
	secs = seconds; /* copy to static var */

	switch (sbd_state)
	{
		case SBD_STATE_CFG_REQ:
		case SBD_STATE_CFG_REQ_WAIT:
			sbd_modem_cfg_update();
			break;

		case SBD_STATE_RSSI_REQ:
		case SBD_STATE_RSSI_REQ_WAIT:
			sbd_get_rssi_update();
			break;

		case SBD_STATE_SEND_TXT_CPY:
		case SBD_STATE_SEND_TXT_CPY_WAIT:
		case SBD_STATE_SEND_TXT_SYNC:
		case SBD_STATE_SEND_TXT_SYNC_WAIT:
			sbd_send_txt_update();
			break;
	}

	switch (sbd_auto)
	{
		case SBD_AUTO_SEND_TXT_CFG:
		case SBD_AUTO_SEND_TXT_CFG_WAIT:
		case SBD_AUTO_SEND_TXT_RSSI:
		case SBD_AUTO_SEND_TXT_SEND:
			sbd_auto_send_txt_update();
			break;
	}
	
	return return_code;
}
/***************************************************************************/

