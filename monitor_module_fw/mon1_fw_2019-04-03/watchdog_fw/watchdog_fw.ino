/***************************************************************************
# EWB Monitor (WATCHDOG) Firmware
# Copyright (c) 2018-2019, Kjeld Jensen <kj@iug.dk> <kj@kjen.dk>
# Engineers Without Borders - Denmark http://iug.dk
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
# Revision
# 2018-09-03 KJ First released version
# 2019-02-26 KJ Code cleanup. Changed sleep interval to 1s. Minor changes 
#               to LED blink. Added serial debug support.
#
#****************************************************************************

# Board: "Arduino Pro or Pro Mini"
# Processor: "ATmega328P (3.3V, 8 MHz)"
# Please notice that at Gandorhun the PCB is base on non-PU chips causing 
# bootloader installation errors.

****************************************************************************/
/* settings and parameters */

#define FW_VERSION "FW 2019-02-26" 

/* PCB type */
#define PCB_03-2018 /* reset pin is activated when the logic level is high */

/* send debug info via serial port */
#define DEBUG_ENABLE /* DEBUG_ENABLE or undefined if debug is disabled */
/* #define DEBUG_SOFTWARE_SERIAL */ /* DEBUG_SOFTWARE_SERIAL or undefined for hw serial port*/

/* timeouts */
/* minimum time [s] before reset of MONITOR occurs */
#define RESET_AFTER  (23 * 3600UL) // > 23 hours

/* maximum time [s] before reset of MONITOR occurs */
#define RESET_BEFORE (25 * 3600UL) // < 25 hours

/***************************************************************************/
/* includes, defines and global variables */

/* system */
unsigned long time_s_approx; /* time since boot [s] */
char i; /* iterator */

/***************************************************************************/
/* send debug info via serial port */
#ifdef DEBUG_ENABLE
  extern "C"{
    char *debug_tx_s;
  }

  #ifdef DEBUG_SOFTWARE_SERIAL
    #define DEBUG_RX false /*  */
    #define DEBUG_TX false /*  */
  
    #include <SoftwareSerial.h>
    SoftwareSerial debugSS = SoftwareSerial(DEBUG_RX, DEBUG_TX);
    SoftwareSerial *debugSerial = &debugSS;
  #else
    HardwareSerial *debugSerial = &Serial;
  #endif
#endif

/***************************************************************************/
/* low power */
#include "LowPower.h" // https://github.com/rocketscream/Low-Power

/***************************************************************************/
/* led */
#define LED_HEARTBEAT 1 /* weak blink (1ms) */
#define LED_BLINK_INTERVAL 15 /* [s] */

unsigned long led_next_blink; /* blink led next time [s] */

/***************************************************************************/
/* monitor communication */
#define PIN_MON_RST_REQ A4 // (PC4/SDA) connected to MONITOR (PC4/SDA)

/***************************************************************************/
/* monitor reset */
#define PIN_MON_RST 7 // (PD7) connected to MONITOR (RESET)
char rst_req; /* request received from MONITOR */
char rst_exec; /* conditions for reset met */ 

/***************************************************************************/
void debug_init(void)
{
  #ifdef DEBUG_ENABLE
    debug_tx_s = malloc (100);
  #endif
}
/***************************************************************************/
void debug_tx(char *text)
{
  #ifdef DEBUG_ENABLE
    debugSerial->begin(57600);
    debugSerial->print (time_s_approx);
    debugSerial->print (" ");
    debugSerial->println(text);
    debugSerial->end();
  #endif
}
/***************************************************************************/
void timer_reset(void)
{
  time_s_approx = 0;
  led_next_blink = LED_BLINK_INTERVAL;
}
/***************************************************************************/
void led_init(void)
{
  digitalWrite(LED_BUILTIN, LOW);
  pinMode(LED_BUILTIN, OUTPUT);
  led_next_blink = LED_BLINK_INTERVAL;
}
/***************************************************************************/
void led_update(void)
{
  if (time_s_approx >= led_next_blink)
  {
    led_blink();
    led_next_blink += LED_BLINK_INTERVAL;
  } 
}
/***************************************************************************/
void led_blink(void)
{
  digitalWrite(LED_BUILTIN, HIGH);
  delay (LED_HEARTBEAT);
  digitalWrite(LED_BUILTIN, LOW);

  debug_tx ("LED blink");  
}
/***************************************************************************/
void led_blink_n(char n)
{
  for (i=0; i<n; i++)
  {
    led_blink();
    if (i<n-1)
      delay (300);
  }
}
/***************************************************************************/
void monitor_reset_init(void)
{
  /* for now the request is received as a digital logic level low */
  pinMode(PIN_MON_RST_REQ, INPUT_PULLUP);

  /* perform a proper reset of the MONITOR */
  monitor_reset_execute();
}
/***************************************************************************/
void monitor_reset_update(void)
{
  rst_exec = 0;
  
  /* check for reset request from MONITOR */
  rst_req = (digitalRead(PIN_MON_RST_REQ) == 0);
  if (rst_req)
  {
    debug_tx ("Reset request");
    if (time_s_approx >= RESET_AFTER)
    {
      rst_exec = 1;
    }
    else
    {
      debug_tx ("Reset invalid");    
    }
  }

  /* check if reset must be enforced */
  else if (time_s_approx >= RESET_BEFORE)
  {
    debug_tx ("Reset timeout");
    rst_exec = 1;
  }
  
  /* if a condition for reset of the MONITOR has been met */
  if (rst_exec == 1)
  {
    debug_tx ("Reset exec");
    monitor_reset_execute();
    timer_reset();
  } 
}
/***************************************************************************/
void monitor_reset_execute()
{
  pinMode(PIN_MON_RST, OUTPUT);
  #ifdef PCB_03-2018
    digitalWrite(PIN_MON_RST, HIGH);
    delay (250);
    digitalWrite(PIN_MON_RST, LOW);
  #else
    digitalWrite(PIN_MON_RST, LOW);
    delay (250);
    digitalWrite(PIN_MON_RST, HIGH);
  #endif
  pinMode(PIN_MON_RST, INPUT);
}
/***************************************************************************/
void setup()
{
  timer_reset(); /* reset 1s timer */
  debug_init(); /* initialize serial debug */
  led_init(); /* initialize LED */
  monitor_reset_init(); /* initialize the monitor reset function */

  #ifdef DEBUG_ENABLE
    debugSerial->begin(57600);
    debugSerial->println("");
    debugSerial->println("EWB Monitor (WATCHDOG)");
    debugSerial->println(FW_VERSION);
    debugSerial->end();
  #endif

  led_blink_n(10); /* blink the LED a few times */
  delay (2000); /* Keep the board awake for a few extra secs to ease flashing */
}
/***************************************************************************/
void loop() 
{
  time_s_approx++; /* get the updated time [s] */

  led_update();
  monitor_reset_update();
    
  // Enter power down state for 1s with ADC and BOD module disabled
  LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);  
}
/***************************************************************************/

