/***************************************************************************
# WASH Monitor 1 (WATCHDOG) Firmware
# Copyright (c) 2018, Kjeld Jensen <kj@iug.dk> <kj@kjen.dk>
# Engineers Without Borders - Denmark http://iug.dk
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
# Revision
# 2018-09-03 KJ First released version
#****************************************************************************

# Board: "Arduino Pro or Pro Mini"
# Processor: "ATmega328P (3.3V, 8 MHz)"
# Please notice that at Gandarhun a prototype PCB based on Arduino Prob Mini
# (ATMega328, 16 MHz) is used.
# Please notice that at Nomo Faama the PCB is base on non-PU chips causing 
# bootloader installation errors.
# Remember to remove connector from Iridium modem before programming 

****************************************************************************/
/* parameters */

#define PCB_03-2018

//#define RESET_AFTER_8S 75   // TEST: =10m
//#define RESET_BEFORE_8S 225 // TEST: =30m

// theoretically: 24h is 10800*8s = 86400s
// in practice almost 10800 is almost 26h, thus 1h is approx. 415
#define RESET_AFTER_8S   9130 // < 22 hours
#define RESET_BEFORE_8S 10900 // > 26 hours


#include "LowPower.h" // https://github.com/rocketscream/Low-Power

#define PIN_MON_RST_REQ A4 // (PC4/SDA) connected to MONITOR (PC4/SDA)
#define PIN_MON_RST 7 // (PD7) connected to MONITOR (RESET)

char i;
unsigned long rst_cnt_8s;
char rst_req;

void setup()
{
  // setup pins
  digitalWrite(LED_BUILTIN, LOW);
  pinMode(LED_BUILTIN, OUTPUT); 
  pinMode(PIN_MON_RST_REQ, INPUT_PULLUP); 
  
  // blink the LED a few times 
  // and keep the board awake for a few secs to ease reprogramming
  blink_led_n(12);
  delay (1000);

  // perform a proper reset of the MONITOR
  reset_mon();
}

void blink_led(void)
{
  digitalWrite(LED_BUILTIN, HIGH);
  delay (1);
  digitalWrite(LED_BUILTIN, LOW);
}

void blink_led_n(char n)
{
  for (i=0; i<n; i++)
  {
    blink_led();
    delay (500);
  }
}

void reset_mon()
{
	rst_cnt_8s = 0;
  pinMode(PIN_MON_RST, OUTPUT);
  #ifdef PCB_03-2018
    digitalWrite(PIN_MON_RST, HIGH);
    delay (250);
    digitalWrite(PIN_MON_RST, LOW);
  #else
    digitalWrite(PIN_MON_RST, LOW);
    delay (250);
    digitalWrite(PIN_MON_RST, HIGH);
  #endif
  pinMode(PIN_MON_RST, INPUT);
}

void loop() 
{
  // Enter power down state for 8s with ADC and BOD module disabled
  LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);  

  // blink the led once
  blink_led();

  // update 8s counter
  rst_cnt_8s += 1; 

	// check for reset request from MONITOR
	rst_req = (digitalRead(PIN_MON_RST_REQ) == 0);
	
  // if timeout or MONITOR requested reset then reset the MONITOR
  if (rst_cnt_8s >= RESET_BEFORE_8S || (rst_req && rst_cnt_8s >= RESET_AFTER_8S))
  {
    reset_mon();
  }
}

