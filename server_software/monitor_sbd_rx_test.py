#!/usr/bin/python


URL = ""

import requests
import time
import datetime

def str2hex(s):
	return s.encode("hex")

def send_sbd_entry(sbd):
	sent_str = datetime.datetime.fromtimestamp(sbd['sent']).replace(microsecond=0).isoformat(' ')
	data_encoded_str = sbd['data'].encode("hex")

	post_data = {'imei': sbd['imei'], 'momsn': sbd['seq'], 'transmit_time': sent_str, 'iridium_latitude': sbd['lat'], 'iridium_longitude': sbd['lon'], 'iridium_cep': sbd['accuracy']/1000, 'data': data_encoded_str}
	print post_data

	r = requests.post(URL, data=post_data)
	print(r.status_code, r.reason)
	print(r.text)

SBD_TEST_DATA = [
	{
		'sent': time.time(),
		'seq': 0,
		'imei': "1234",
		'lat': 55.38,
		'lon': 12.40,
		'accuracy': 5000,
		'data': "M1,test",
	},
]

send_sbd_entry (SBD_TEST_DATA[0])


