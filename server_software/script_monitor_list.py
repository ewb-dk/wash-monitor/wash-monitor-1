#!/usr/bin/python

monitor_list = [
	{
		'id': 1000,
		'type': "WATERSUPPLY_EWB_SAT_V1",
		'sender': "",
		'value': "PROTOCOL,INDEX,BAT1_VOLT_MIN,CAB1_TEMP_MIN,CAB1_TEMP_MAX,AIR_TEMP1_MIN,AIR_TEMP1_MAX,WATER_VOL1,WATER_TEMP1_MIN,WATER_TEMP1_MAX",
		'scale': ",,10,10,10,10,10,1,10,10",
		'unit': ",,Volt,Celcius,Celcius,Celcius,Celcius,Liter,Celcius,Celcius",
		'name': "Test",
		'location': "Odense, DK",
		'lat': 55.37840,
		'lon': 10.34440,
		'time_zone': "Europe/Copenhagen",
		'owner': "EWB",
		'pin': "1024",
		'report_daily': "",
		'report_weekly': "",
	},
]

