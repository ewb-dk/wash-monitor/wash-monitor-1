#!/usr/bin/python

monitor_id = 1000


# sudo apt install python-mysql.connector
import mysql.connector
from ewb_settings import *
from monitor_list import *

for i in range(len(monitor_list)):
	if monitor_list[i]['id'] == monitor_id:
		print 'Type:', monitor_list[i]['type']
		print 'Sender:', monitor_list[i]['sender']
		print 'Value:', monitor_list[i]['value']
		print 'Scale:', monitor_list[i]['scale']
		print 'Unit:', monitor_list[i]['unit']
		print 'Name:', monitor_list[i]['name']
		print 'Location:', monitor_list[i]['location']
		print 'Lat:', monitor_list[i]['lat']
		print 'Lon:', monitor_list[i]['lon']
		print 'Time zone:', monitor_list[i]['time_zone']
		print 'Owner:', monitor_list[i]['owner']
		print 'Pin:', monitor_list[i]['pin']
		print 'Report_daily:', monitor_list[i]['report_daily']
		print 'Report_weekly', monitor_list[i]['report_weekly']

		cnx = mysql.connector.connect(user=EWB_USER, database=EWB_DB, password=EWB_PASS, port=EWB_PORT)
		cursor = cnx.cursor()

		cursor.execute("UPDATE monitor_list SET type = '%s' WHERE id = '%s'"% (monitor_list[i]['type'], monitor_id))
		cursor.execute("UPDATE monitor_list SET sender = '%s' WHERE id = '%s'"% (monitor_list[i]['sender'], monitor_id))
		cursor.execute("UPDATE monitor_list SET value = '%s' WHERE id = '%s'"% (monitor_list[i]['value'], monitor_id))
		cursor.execute("UPDATE monitor_list SET scale = '%s' WHERE id = '%s'"% (monitor_list[i]['scale'], monitor_id))
		cursor.execute("UPDATE monitor_list SET unit = '%s' WHERE id = '%s'"% (monitor_list[i]['unit'], monitor_id))
		cursor.execute("UPDATE monitor_list SET name = '%s' WHERE id = '%s'"% (monitor_list[i]['name'], monitor_id))
		cursor.execute("UPDATE monitor_list SET location = '%s' WHERE id = '%s'"% (monitor_list[i]['location'], monitor_id))
		cursor.execute("UPDATE monitor_list SET lat = '%s' WHERE id = '%s'"% (monitor_list[i]['lat'], monitor_id))
		cursor.execute("UPDATE monitor_list SET lon = '%s' WHERE id = '%s'"% (monitor_list[i]['lon'], monitor_id))
		cursor.execute("UPDATE monitor_list SET time_zone = '%s' WHERE id = '%s'"% (monitor_list[i]['time_zone'], monitor_id))
		cursor.execute("UPDATE monitor_list SET owner = '%s' WHERE id = '%s'"% (monitor_list[i]['owner'], monitor_id))
		cursor.execute("UPDATE monitor_list SET pin = '%s' WHERE id = '%s'"% (monitor_list[i]['pin'], monitor_id))
		cursor.execute("UPDATE monitor_list SET report_daily = '%s' WHERE id = '%s'"% (monitor_list[i]['report_daily'], monitor_id))
		cursor.execute("UPDATE monitor_list SET report_weekly = '%s' WHERE id = '%s'"% (monitor_list[i]['report_weekly'], monitor_id))

		cnx.commit()
		cursor.close()
		cnx.close()


